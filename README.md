# UnfoldingDemo

Python script and Jupyter notebook for running toy unfolding analysis

```unfolding_demo.ipynb``` lets you run through the unfolding procedure, while 
```toy_unfolding.py``` contains helper functions for generating toy events
and computing the response matrix