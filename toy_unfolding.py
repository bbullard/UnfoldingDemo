import numpy as np
import scipy.stats as st
  
# Define background distribution
def bkg(x):
  c = 1.
  return np.exp(-c*x)/(1-(1/c)*np.exp(-c))

# Define signal distribution
def sig(x):
  M = 0.55
  W = 0.15
  gamma = np.sqrt(M**2*(M**2+W**2))
  k = 2*np.sqrt(2)*M*W*gamma/(np.pi*np.sqrt(M**2+gamma))
  return k/((x**2-M**2)**2 + M**2*W**2)

class bkg_pdf(st.rv_continuous):
  def _pdf(self, x):
    return bkg(x)

class sig_pdf(st.rv_continuous):
  def _pdf(self, x):
    return sig(x)

class toy_unfolding:

  def __init__(self, xLow, xHigh, nBins, nSig, nBkg, smearing = 0.1, shift = 0,  nLin=1000):
    self.xLow = xLow
    self.xHigh = xHigh
    self.nBins = nBins
    self.nSig = nSig
    self.nBkg = nBkg
    self.smearing = smearing
    self.shift = shift

    # Make array of x-values for truth distributions
    self.xTruth = np.linspace(self.xLow, self.xHigh, nLin)

  # Sample the signal distribution
  def sample_signal(self):
    sig_distribution = sig_pdf(a=self.xLow, b=self.xHigh, name="signal_distribution")
    self.sig_sample = sig_distribution.rvs(size = self.nSig)
    self.sig_sample_smeared = self.sig_sample + np.random.normal(self.shift, self.smearing, self.nSig)

  # Sample the background distribution
  def sample_bkg(self):
    bkg_distribution = bkg_pdf(a=self.xLow, b=self.xHigh, name="background_distribution")
    self.bkg_sample = bkg_distribution.rvs(size = self.nBkg)
    self.bkg_sample_smeared = self.bkg_sample + np.random.normal(self.shift, self.smearing, self.nBkg)

  # Get total samples
  def GetSampleHist(self):
    all_sample = np.append(self.bkg_sample_smeared, self.sig_sample_smeared)
    bincontent, binedges = np.histogram(all_sample, self.nBins, (self.xLow, self.xHigh) )
    bincenters = np.array([(binedges[i]+binedges[i+1])/2 for i in range(len(binedges)-1)])

    return bincenters, bincontent
  
  # Get x-values
  def GetXArray(self):
    return self.xTruth

  # Get truth background spectrum
  def GetBkgTruth(self):
    return bkg(self.xTruth)*self.nBkg / self.nBins

  # Get truth signal spectrum
  def GetSigTruth(self):
    return sig(self.xTruth)*self.nSig / self.nBins
  
  # Get smeared truth signal spectrum
  def GetSmearedTruth(self):
    truth = self.GetSigTruth()+self.GetBkgTruth()
    if self.smearing == 0:
      return truth
    # Negate the shift parameter because the convolution negates the second function
    gauss = 1/(self.smearing*np.sqrt(2*np.pi)) * np.exp(-0.5 * ((self.xTruth-0.5-self.shift)/self.smearing)**2) / len(self.xTruth)
    return np.convolve(truth, gauss, 'same')

  # Get assimov dataset
  def GetAssimov(self):
    bincontent, binedges = np.histogram(self.bkg_sample, self.nBins, (self.xLow, self.xHigh))
    bincenters = np.array([(binedges[i]+binedges[i+1])/2 for i in range(len(binedges)-1)])
    bincontent_assimov = (self.nSig*sig(bincenters) + self.nBkg*bkg(bincenters))/self.nBins

    return bincenters, bincontent_assimov

  def GetAssimovFluctuation(self, ibin, sigma = 1):
    bincontent, binedges = np.histogram(self.bkg_sample, self.nBins, (self.xLow, self.xHigh))
    bincenters = np.array([(binedges[i]+binedges[i+1])/2 for i in range(len(binedges)-1)])
    bincontent_assimov = (self.nSig*sig(bincenters) + self.nBkg*bkg(bincenters))/self.nBins
    if (sigma == 0):
      return bincenters, bincenters_assimov
    y = bincontent_assimov[ibin]
    sign = sigma/abs(sigma)
    bincontent_assimov[ibin] = 0.5*( 2*y + sign*np.sqrt(4*y*sigma**2+sigma**4) + sigma**2)

    return bincenters, bincontent_assimov
    
  def GetMigrationMatrix(self, nSamples=int(5e4)):
    if self.nBkg < nSamples:
      dist = bkg_pdf(a=self.xLow, b=self.xHigh, name="background_distribution")
      truth = dist.rvs(size = nSamples)
      reco = truth + np.random.normal(self.shift, self.smearing, nSamples)

      migration_matrix, xedges, yedges = np.histogram2d(truth, reco, self.nBins, [[self.xLow, self.xHigh],[self.xLow, self.xHigh]])
    else:
      migration_matrix, xedges, yedges = np.histogram2d(self.bkg_sample, self.bkg_sample_smeared, self.nBins, [[self.xLow, self.xHigh],[self.xLow, self.xHigh]])

    for i in range(self.nBins):
      if not np.sum(migration_matrix[i]) <= 0:
        migration_matrix[i] = migration_matrix[i]/np.sum(migration_matrix[i])
  
    return migration_matrix
